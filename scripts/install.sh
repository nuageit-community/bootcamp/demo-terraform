#!/usr/bin/env bash

TERRAFORM_VERSION="1.0.2"
TMP_DIR="$(mktemp -d)"

echo "📝 Change Directory"
cd $TMP_DIR

echo "✨ Install Terraform"
wget https://releases.hashicorp.com/terraform/"$TERRAFORM_VERSION"/terraform_"$TERRAFORM_VERSION"_linux_amd64.zip
unzip terraform_"$TERRAFORM_VERSION"_linux_amd64.zip
mv terraform /usr/local/bin
terraform --version

echo "✨ Install TFSec"
wget https://github.com/aquasecurity/tfsec/releases/download/v0.48.7/tfsec-linux-amd64
mv tfsec-linux-amd64 tfsec
chmod a+x tfsec
mv tfsec /usr/local/bin
tfsec --version

echo "✨ Install Terraform Docs"
wget https://github.com/terraform-docs/terraform-docs/releases/download/v0.14.1/terraform-docs-v0.14.1-linux-amd64.tar.gz
tar -xvf terraform-docs-v0.14.1-linux-amd64.tar.gz
mv terraform-docs /usr/local/bin
terraform-docs --version

cd -
