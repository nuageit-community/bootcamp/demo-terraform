# Semantic Versioning Changelog

## 1.0.0 (2021-07-18)


### :sparkles: News

* add docs script, readme terraform and others ([98cc600](https://gitlab.com/bootcamp-stefanini/demo-terraform/commit/98cc600ee30ec0f34385395faa871647871cd131))
* add install tfsec ([6a989e4](https://gitlab.com/bootcamp-stefanini/demo-terraform/commit/6a989e43b61964ddf6c3a796da0e87d55cd8a763))
* **setup:** initial commit project ([2dd9cd4](https://gitlab.com/bootcamp-stefanini/demo-terraform/commit/2dd9cd4953a2a959733b20b502a397671665edfe))
