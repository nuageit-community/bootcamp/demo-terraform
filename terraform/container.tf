resource "docker_container" "application" {
  name         = "application"
  image        = docker_image.application.latest
  must_run     = true
  rm           = true
  privileged   = true
  network_mode = var.docker_network
  env          = var.docker_envs
  ports {
    internal = var.docker_internal_port
    external = var.docker_external_port
  }
}
