variable "docker_internal_port" {
  type        = number
  default     = 8080
  description = "Docker container internal port"
}

variable "docker_external_port" {
  type        = number
  default     = 8080
  description = "Docker container external port"
}

variable "docker_envs" {
  type        = list(any)
  default     = []
  description = "Docker container list environment variables"
}

variable "docker_network" {
  type        = string
  default     = "application"
  description = "Docker container network"
}
