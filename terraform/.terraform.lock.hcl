# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/local" {
  version = "2.1.0"
  hashes = [
    "h1:EYZdckuGU3n6APs97nS2LxZm3dDtGqyM4qaIvsmac8o=",
    "zh:0f1ec65101fa35050978d483d6e8916664b7556800348456ff3d09454ac1eae2",
    "zh:36e42ac19f5d68467aacf07e6adcf83c7486f2e5b5f4339e9671f68525fc87ab",
    "zh:6db9db2a1819e77b1642ec3b5e95042b202aee8151a0256d289f2e141bf3ceb3",
    "zh:719dfd97bb9ddce99f7d741260b8ece2682b363735c764cac83303f02386075a",
    "zh:7598bb86e0378fd97eaa04638c1a4c75f960f62f69d3662e6d80ffa5a89847fe",
    "zh:ad0a188b52517fec9eca393f1e2c9daea362b33ae2eb38a857b6b09949a727c1",
    "zh:c46846c8df66a13fee6eff7dc5d528a7f868ae0dcf92d79deaac73cc297ed20c",
    "zh:dc1a20a2eec12095d04bf6da5321f535351a594a636912361db20eb2a707ccc4",
    "zh:e57ab4771a9d999401f6badd8b018558357d3cbdf3d33cc0c4f83e818ca8e94b",
    "zh:ebdcde208072b4b0f8d305ebf2bfdc62c926e0717599dcf8ec2fd8c5845031c3",
    "zh:ef34c52b68933bedd0868a13ccfd59ff1c820f299760b3c02e008dc95e2ece91",
  ]
}

provider "registry.terraform.io/kreuzwerker/docker" {
  version = "2.14.0"
  hashes = [
    "h1:tvLqMbU4X1QkpaTGjJkpHDJq7yq18aOxYcBzwJmUp3s=",
    "zh:2c79f6e150c6f638310f4d52dcc789a112ac76fb3c6173fb1db9fae95d32a7f0",
    "zh:35564e66530b7a638a56dd885b9a5063c366d1e25f81a5e27da8ae6660065109",
    "zh:3e7e17c3966473761d463a21839e9d1d2b9cedfc7ac473e84fdb544c4e6aecc6",
    "zh:4f1392a6c87980b72610a021c6af68dbbc0ab244165bd5406b8389906b9f6534",
    "zh:5a7dd578e340b022d14447ff675178448c38ac7cc322a214538d50ae79c29755",
    "zh:6765087dee91cfd9f65d2898bb241b8f52fbbb2a38bf5d1a6352c14b3b018aa0",
    "zh:6de36a1f3caae9142649aa6bc22f026dc5c9a80cca0e209c7f1e0609611dac63",
    "zh:8cd67d4994c19ec8bf4c7a4dff7d6874e7731b00a0c7409418e311867da03680",
    "zh:b7c96138d822c021dc939ff7e2fde1e3038b21d6517179717161f5d4298b5907",
    "zh:baed83fed67972fdce68b82713ab50714fc0086cd0e2bf979e3805cc3e9dcaf9",
    "zh:bee1bb1c4b7c33237a0aa90b5eb9dd63c99848e561b999293c8ab5694eedc80a",
    "zh:def0856b1df1b74b947b57c2af4b51c3941fe5c840aa80818c9a57ea12408f26",
    "zh:e6c6eafca3da3aab71e58535f547607fa40e6be06463332e4e42c4917a3ee6d9",
  ]
}
