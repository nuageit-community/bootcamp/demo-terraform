## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13 |
| <a name="requirement_docker"></a> [docker](#requirement\_docker) | 2.14.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_docker"></a> [docker](#provider\_docker) | 2.14.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [docker_container.application](https://registry.terraform.io/providers/kreuzwerker/docker/2.14.0/docs/resources/container) | resource |
| [docker_image.application](https://registry.terraform.io/providers/kreuzwerker/docker/2.14.0/docs/resources/image) | resource |
| [docker_network.application](https://registry.terraform.io/providers/kreuzwerker/docker/2.14.0/docs/resources/network) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_docker_envs"></a> [docker\_envs](#input\_docker\_envs) | Docker container list environment variables | `list(any)` | `[]` | no |
| <a name="input_docker_external_port"></a> [docker\_external\_port](#input\_docker\_external\_port) | Docker container external port | `number` | `8080` | no |
| <a name="input_docker_internal_port"></a> [docker\_internal\_port](#input\_docker\_internal\_port) | Docker container internal port | `number` | `8080` | no |
| <a name="input_docker_network"></a> [docker\_network](#input\_docker\_network) | Docker container network | `string` | `"application"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_network_id"></a> [network\_id](#output\_network\_id) | n/a |
| <a name="output_network_name"></a> [network\_name](#output\_network\_name) | n/a |
